const app=require("../index");
const supertest=require("supertest");

test ("El URL de productos retorna un solo producto con el nombre Producto 1", async()=>{
    await supertest(app)
        .get("/productos-json")
        .expect(200)
        .then ((response) => {
            expect(response.status).toBe(200);
            expect(response.headers["content-type"]).toEqual(
                "application/json; charset=utf-8"
            );
            expect(response.body).toHaveLength(6);
            expect(response.body[0]).toHaveProperty("nombre", "Lavarropas");

            expect(response.body).toHaveLength(6);
            const length=response.body.length
            expect(response.body[5]).toHaveProperty("nombre", "Ventilador Midea")

        });
    });